import styles from "./Button.module.scss";

export default function Button({
	onClick,
	Icon,
	color = "primary",
	width = "auto",
	children,
	disabled = false,
}) {
	return (
		<button
			className={`${styles.button} ${styles[color]}`}
			style={{ width: width }}
			onClick={onClick}
			disabled={disabled}
		>
			{Icon && <Icon style={styles.svg} />}
			<span>{children}</span>
		</button>
	);
}
