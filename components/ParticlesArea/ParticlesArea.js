import React, { useCallback } from "react";
import Particles from "react-tsparticles";
import { loadFull } from "tsparticles";
import { screenSuperiorOf } from "../../utils/utils";
import lineParticlesDatas from "./line-particles-datas.json";
import lineParticlesMobileDatas from "./line-particles-mobile-datas.json";

function ParticlesArea() {
	const particlesInit = useCallback(async (engine) => {
		// you can initiate the tsParticles instance (engine) here, adding custom shapes or presets
		// this loads the tsparticles package bundle, it's the easiest method for getting everything ready
		// starting from v2 you can add only the features you need reducing the bundle size
		await loadFull(engine);
	}, []);

	const particlesLoaded = useCallback(async (container) => {}, []);
	return (
		<div className="h-full relative">
			{/* @ts-ignore */}
			{screenSuperiorOf(768) ? (
				<Particles
					id="tsparticles"
					init={particlesInit}
					loaded={particlesLoaded}
					width="99vw"
					className="absolute  left-0 top-0 margin-0 padding-0 z-0"
					options={lineParticlesDatas}
				/>
			) : (
				<Particles
					id="tsparticles"
					init={particlesInit}
					loaded={particlesLoaded}
					width="99vw"
					className="absolute  left-0 top-0 margin-0 padding-0 z-0"
					options={lineParticlesMobileDatas}
				/>
			)}
		</div>
	);
}

export default ParticlesArea;
