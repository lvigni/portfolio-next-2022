import { useState } from "react";
import ChevronUp from "../svg/ChevronUp";
import styles from "./AccordionCard.module.scss";
import { AnimatePresence, motion } from "framer-motion";
import { screenSuperiorOf } from "../../utils/utils";

function AccordionCard({
	title,
	duration,
	years,
	setIsOpen,
	isOpen,
	children,
}) {
	return (
		<div className={styles.card}>
			<div className={styles.header} onClick={setIsOpen}>
				<div className={styles.part}>
					<span className={styles.name}>{title}</span>
					<span className={styles.duration}>{duration}</span>
				</div>
				<div className={styles.part}>
					{screenSuperiorOf(480) && (
						<span className={styles.years}>{years}</span>
					)}
					<ChevronUp
						style={`${styles.chevron} ${isOpen ? styles.isOpen : ""}`}
					/>
				</div>
			</div>
			<AnimatePresence>
				{isOpen && (
					<motion.div
						key={`content-accordion-${title}-${years}`}
						initial={{ height: 0, opacity: 0 }}
						animate={{ height: "auto", opacity: 1 }}
						exit={{ height: 0, opacity: 0 }}
						transition={{ duration: 0.3 }}
					>
						<div className={styles.content}>{children}</div>
					</motion.div>
				)}
			</AnimatePresence>
		</div>
	);
}

export default AccordionCard;
