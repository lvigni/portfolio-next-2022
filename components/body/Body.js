import Head from "next/head";
import { useState } from "react";

export default function Body({ children, title = "Vigni Lucas - Immersion" }) {
	return (
		<>
			<Head>
				<title>{title}</title>
				<meta
					name="description"
					content="Je suis Lucas Vigni, développeur Web & Mobile passionné, vennez découvrir mon portfolio grâce à une immersion.. différente."
				/>
				<meta name="viewport" content="width=device-width, initial-scale=1" />
				<link rel="icon" href="/favicon.ico" />
			</Head>
			<main>{children}</main>
		</>
	);
}
