import styles from "./GameIntro.module.scss";

export default function GameIntro({ children }) {
	return (
		<div className={styles.introContainer}>
			<div className={styles.intro}>{children}</div>
		</div>
	);
}
