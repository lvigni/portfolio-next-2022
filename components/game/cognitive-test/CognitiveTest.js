import { useEffect, useState } from "react";
import GameIntro from "../../../components/game-intro/GameIntro";
import BottomArrowIcon from "../../../components/svg/BottomArrowIcon";
import LeftArrowIcon from "../../../components/svg/LeftArrowIcon";
import RightArrowIcon from "../../../components/svg/RightArrowIcon";
import TopArrowIcon from "../../../components/svg/TopArrowIcon";
import { randomNumber } from "../../../utils/utils";
import styles from "./CognitiveTest.module.scss";
import stylesIntro from "../../../components/game-intro/GameIntro.module.scss";
import Button from "../../../components/Button/Button";

export default function CognitiveTest({
	raiseValueLoader,
	nextStepGame,
	skipGame,
}) {
	const [hasStarted, _setHasStarted] = useState(false);
	const [arrowDirection, _setArrowDirection] = useState();
	const [color, _setColor] = useState();
	const [combo, _setCombo] = useState(0);
	const [timeStartCombo, _setTimeStartCombo] = useState();
	const [isFinished, setIsFinished] = useState(false);
	const [timerEnd, setTimerEnd] = useState();

	let arrowDirectionRef = arrowDirection;
	let colorRef = color;
	let hasStartedRef = hasStarted;
	let comboRef = combo;
	let timeStartComboRef = timeStartCombo;

	const setArrowDirection = (data) => {
		arrowDirectionRef = data;
		_setArrowDirection(data);
	};

	const setColor = (data) => {
		colorRef = data;
		_setColor(data);
	};

	const setHasStarted = (data) => {
		hasStartedRef = data;
		_setHasStarted(data);
	};

	const setCombo = (data) => {
		comboRef = data;
		_setCombo(data);
	};

	const setTimeStartCombo = (data) => {
		timeStartComboRef = data;
		_setTimeStartCombo(data);
	};
	const positions = ["ArrowUp", "ArrowRight", "ArrowDown", "ArrowLeft"];

	const resetGame = () => {
		setHasStarted(false);
		setCombo(0);
		document.removeEventListener("keyup", onKeyArrow, true);
	};

	const onKeySpace = (key) => {
		if (key.code === "Space") {
			setTimerEnd(null);
			setHasStarted(true);
			generateArrow();
			document.removeEventListener("keyup", onKeySpace, true);
		}
	};

	const onKeyArrow = (key) => {
		const keyPressed = key.code;
		if (
			["ArrowLeft", "ArrowRight", "ArrowUp", "ArrowDown"].includes(keyPressed)
		) {
			const precArrowDirection = arrowDirectionRef;
			const isCorrectKey =
				colorRef === "red"
					? keyPressed === precArrowDirection
					: isOposalArrow(precArrowDirection, keyPressed);
			if (isCorrectKey) {
				const newComboScore = comboRef + 1;
				if (newComboScore !== 20) {
					if (comboRef === 0) {
						setTimeStartCombo(new Date().getTime());
					}
					generateArrow(precArrowDirection);
					setCombo(newComboScore);
				} else {
					const end = new Date().getTime();
					const timeDiff = end - timeStartComboRef;
					setTimerEnd(timeDiff);
					if (timeDiff < 14000) {
						setIsFinished(true);
						raiseValueLoader();
					} else resetGame();
				}
			} else resetGame();
		}
	};

	useEffect(() => {
		!hasStartedRef && document.addEventListener("keyup", onKeySpace, true);
		hasStartedRef ? document.addEventListener("keyup", onKeyArrow, true) : null;
	}, [hasStarted]);

	const generateArrow = (precArrowDirection = null) => {
		let newPositions = precArrowDirection
			? positions.filter((p) => p !== precArrowDirection)
			: positions;
		let pos = newPositions[randomNumber(0, precArrowDirection ? 2 : 3)];
		setArrowDirection(pos);
		setColor(randomNumber(0, 1) === 1 ? "red" : "white");
	};

	const isOposalArrow = (key, direction) => {
		switch (direction) {
			case "ArrowLeft":
				return key === "ArrowRight";
			case "ArrowRight":
				return key === "ArrowLeft";
			case "ArrowUp":
				return key === "ArrowDown";
			case "ArrowDown":
				return key === "ArrowUp";
		}
	};
	const getArrowSvg = () => {
		switch (arrowDirection) {
			case "ArrowUp":
				return <TopArrowIcon style={`${styles[color]} ${styles.svg}`} />;
			case "ArrowRight":
				return <RightArrowIcon style={`${styles[color]} ${styles.svg}`} />;
			case "ArrowDown":
				return <BottomArrowIcon style={`${styles[color]} ${styles.svg}`} />;
			case "ArrowLeft":
				return <LeftArrowIcon style={`${styles[color]} ${styles.svg}`} />;

			default:
				return "";
		}
	};
	return (
		<div className={styles.cognitiveSection}>
			<div className={styles.nextLevel}>
				<Button
					Icon={RightArrowIcon}
					color="primary"
					onClick={() => {
						skipGame();
						nextStepGame();
					}}
				>
					Passer au jeu suivant
				</Button>
			</div>
			<h1 className={`${styles.titleStepGame}`}>Faisceau pyramidal</h1>
			<div className={styles.cognitive}>
				<GameIntro>
					<div className={stylesIntro.block}>
						<span className={stylesIntro.label}>Commandes: </span>
						<p className={stylesIntro.value}>Touches directionnelles</p>
					</div>
					<div className={stylesIntro.block}>
						<span className={stylesIntro.label}>Consigne: </span>
						<div className={stylesIntro.value}>
							<p>
								Si une flèche orange apparait, appuie sur ta touche dans la même
								direction
							</p>
							<p>Si elle est blanche, appuie dans la direction opposée</p>
						</div>
					</div>
					<div className={stylesIntro.block}>
						<span className={stylesIntro.label}>Objectif: </span>
						<div className={stylesIntro.value}>
							<p>20 combos en moins de 14s</p>
						</div>
					</div>
				</GameIntro>
				<div className={styles.cognitiveGame}>
					{hasStarted && !timerEnd && (
						<div className={styles.cognitiveGameContent}>
							<div className={`${styles.comboSection}`}>
								<span className="x">x</span>
								<span
									className={`${styles.combo} ${
										combo >= 10 ? styles.fire : ""
									}`}
								>
									{combo}
								</span>
							</div>
							{getArrowSvg()}
						</div>
					)}
					{!hasStarted && !isFinished && (
						<span className={styles.infoStartGame}>
							Appuie sur
							<span className={styles.important}> "Espace" </span>
							pour commencer.
						</span>
					)}
					{timerEnd && (
						<div className={styles.infoEndGame}>
							{!isFinished && <span className={styles.title}>Dommage..</span>}
							<span className={styles.value}>{` ${timerEnd
								.toString()
								.slice(0, 2)}'${timerEnd.toString().slice(2, 4)}s`}</span>
							{isFinished && (
								<Button
									Icon={RightArrowIcon}
									color="secondary"
									onClick={nextStepGame}
									width="100%"
								>
									Entrer dans le "Cortex visuel"
								</Button>
							)}
						</div>
					)}
				</div>
			</div>
		</div>
	);
}
