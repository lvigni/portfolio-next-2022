import { useCallback, useMemo, useState } from "react";
import Button from "../../../components/Button/Button";
import DoneIcon from "../../../components/svg/DoneIcon";
import LevelUpIcon from "../../../components/svg/LevelUpIcon";
import PlayIcon from "../../../components/svg/PlayIcon";
import RestartIcon from "../../../components/svg/RestartIcon";
import RightArrowIcon from "../../../components/svg/RightArrowIcon";
import { randomNumber } from "../../../utils/utils";
import styles from "./MemoryNumber.module.scss";
import stylesIntro from "../../../components/game-intro/GameIntro.module.scss";
import GameIntro from "../../../components/game-intro/GameIntro";

export default function MemoryNumber({
	raiseValueLoader,
	nextStepGame,
	skipGame,
}) {
	const [level, setLevel] = useState(0);
	const [isRunning, setIsRunning] = useState(false);
	const [hasStarted, setHasStarted] = useState(false);
	const [stepIndex, setStepIndex] = useState();
	const [completeNumber, setCompleteNumber] = useState();
	const [response, setResponse] = useState("");
	const [statusResponse, setStatusResponse] = useState();

	const nbAvailables = ["0", "1", "2", "3", "4", "5", "6", "7", "8", "9"];

	const launchRandomNumber = (lvl) => {
		setHasStarted(true);
		let index = 0;
		let fullNumber = "";
		for (let i = 0; i < getMaxNbAccordingToLevel(lvl); i++) {
			const hasPrevNumber = fullNumber?.length > 0;
			let newNbAvailables = hasPrevNumber
				? nbAvailables.filter((nb) => {
						return nb !== fullNumber.at(-1);
				  })
				: nbAvailables;
			const newRandomNb =
				newNbAvailables[randomNumber(0, hasPrevNumber ? 8 : 9)];
			fullNumber += newRandomNb;
		}
		setIsRunning(true);
		setCompleteNumber(fullNumber);
		const interval = setInterval(() => {
			if (index !== getMaxNbAccordingToLevel(lvl)) {
				setStepIndex(index);
				index++;
			} else {
				clearInterval(interval);
				setIsRunning(false);
				setStepIndex(null);
			}
		}, 1000);
		return () => {
			clearInterval(interval);
		};
	};

	const getMaxNbAccordingToLevel = (lvl) => {
		switch (lvl) {
			case 1:
				return 4;
			case 2:
				return 5;
			case 3:
				return 6;
			case 4:
				return 7;
		}
	};

	const submitResponse = useCallback(() => {
		if (response.toString() === completeNumber) {
			setStatusResponse("correct");
			raiseValueLoader();
		} else setStatusResponse("wrong");
	}, [response, completeNumber]);

	const resetGeneration = () => {
		setStatusResponse(null);
		setStepIndex(null);
	};

	const restartGame = useCallback(() => {
		resetGeneration();
		setLevel(level);
		launchRandomNumber(level);
	}, [level]);

	const nextLevel = useCallback(() => {
		if (level !== 4) {
			resetGeneration();
			setLevel(level + 1);
			launchRandomNumber(level + 1);
		} else nextStepGame();
	}, [level]);

	const generateNumber = useCallback(() => {
		let numbers = [];
		for (let index = 0; index < getMaxNbAccordingToLevel(level); index++) {
			numbers.push(
				<div
					className={styles.generatedNumber}
					key={`number-generated-${index}-${new Date()}`}
				>
					{stepIndex === index ? completeNumber[stepIndex] : ""}
				</div>
			);
		}
		return numbers;
	}, [stepIndex, isRunning]);

	const displayLevel = () => {
		if (level === 0 || (level === 4 && statusResponse === "correct"))
			return false;
		return true;
	};

	const canSubmit = useMemo(() => {
		return response?.length === completeNumber?.length;
	}, [completeNumber, response]);

	const getErrorNumber = () => {
		return [...completeNumber].map((number, index) => (
			<span
				className={number === response[index] ? styles.correct : styles.wrong}
			>
				{response[index]}
			</span>
		));
	};
	return (
		<div className={styles.memoryNumberSection}>
			<div className={styles.memoryNumber}>
				<GameIntro>
					<div className={styles.nextLevel}>
						<Button
							Icon={RightArrowIcon}
							color="primary"
							onClick={() => {
								skipGame();
								nextStepGame();
							}}
						>
							Passer au jeu suivant
						</Button>
					</div>
					<h1 className={`${styles.titleStepGame}`}>Néocortex</h1>
					<div className={stylesIntro.block}>
						<span className={stylesIntro.label}>Commandes: </span>
						<p className={stylesIntro.value}>Pavet numérique</p>
					</div>
					<div className={stylesIntro.block}>
						<span className={stylesIntro.label}>Consigne: </span>
						<div className={stylesIntro.value}>
							<p>Des chiffres vont apparaître à tour de rôle</p>
							<p>Mémorise les, puis saisis les dans l'ordre</p>
						</div>
					</div>
					<div className={stylesIntro.block}>
						<span className={stylesIntro.label}>Objectif: </span>
						<div className={stylesIntro.value}>
							<p>Terminer le niveau 4</p>
						</div>
					</div>
				</GameIntro>
				<div className={styles.generationContent}>
					{displayLevel() && <p className={styles.level}>Niveau {level}</p>}
					{!hasStarted && !isRunning && (
						<Button
							Icon={PlayIcon}
							color="secondary"
							onClick={() => {
								const newLevel = level + 1;
								setLevel(newLevel);
								launchRandomNumber(newLevel);
							}}
						>
							Commencer
						</Button>
					)}
					{isRunning && completeNumber && (
						<div className={styles.generatedNumbers}>{...generateNumber()}</div>
					)}
					{!isRunning && hasStarted && !statusResponse && (
						<div className={styles.inputResponse}>
							<div className="w-full mb-6 md:mb-0">
								<label
									className="block uppercase tracking-wide text-xs font-bold mb-2"
									htmlFor="grid-first-name"
								>
									Réponse
								</label>
								<input
									className="appearance-none block w-full bg-gray-200 text-gray-700 border border-red-500 rounded py-3 px-4 mb-3 leading-tight focus:outline-none focus:bg-white"
									id="grid-first-name"
									type="text"
									onChange={(e) => setResponse(e.target.value)}
									autoFocus
									onKeyDown={(e) => {
										if (canSubmit && e.key === "Enter") submitResponse();
									}}
								/>
							</div>
							<Button
								Icon={DoneIcon}
								color="secondary"
								onClick={submitResponse}
								width="100%"
								disabled={!canSubmit}
							>
								Valider
							</Button>
						</div>
					)}
					{statusResponse === "correct" && (
						<div className={styles.correctAnswer}>
							{level !== 4 ? (
								<Button
									Icon={LevelUpIcon}
									color="secondary"
									onClick={nextLevel}
									width="100%"
								>
									Niveau suivant
								</Button>
							) : (
								<div className={styles.nextGame}>
									<Button
										Icon={RightArrowIcon}
										color="secondary"
										onClick={nextStepGame}
										width="100%"
									>
										Entrer dans le "Tronc cérébral"
									</Button>
								</div>
							)}
						</div>
					)}
					{statusResponse === "wrong" && (
						<div className={styles.wrongAnswer}>
							<div className={styles.numberReveal}>
								<div className={styles.wrongReveal}>{getErrorNumber()}</div>
								<span className={styles.correctReveal}>{completeNumber}</span>
							</div>
							<Button
								Icon={RestartIcon}
								color="secondary"
								onClick={restartGame}
								width="100%"
							>
								Recommencer
							</Button>
						</div>
					)}
				</div>
			</div>
		</div>
	);
}
