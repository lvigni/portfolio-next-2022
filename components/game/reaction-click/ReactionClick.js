import { useCallback, useMemo, useState } from "react";
import Button from "../../../components/Button/Button";
import GameIntro from "../../../components/game-intro/GameIntro";
import DotsIcon from "../../../components/svg/DotsIcon";
import FlashIcon from "../../../components/svg/FlashIcon";
import RightArrowIcon from "../../../components/svg/RightArrowIcon";
import TimeIcon from "../../../components/svg/TimeIcon";
import { randomNumber } from "../../../utils/utils";
import styles from "./ReactionClick.module.scss";
import stylesIntro from "../../../components/game-intro/GameIntro.module.scss";

export default function ReactionClick({
	raiseValueLoader,
	nextStepGame,
	skipGame,
}) {
	const [hasStarted, setHasStarted] = useState(false);
	const [timerDisplayGreen, setTimerDisplayGreen] = useState();
	const [reactionTime, setReactionTime] = useState(false);
	const [isTooSoon, setIsToSoon] = useState(false);
	const [timeoutGreen, setTimeoutGreen] = useState();
	const [isAllowed, setIsAllowed] = useState(false);

	const launchTimer = () => {
		setHasStarted(true);
		setIsToSoon(false);
		setTimerDisplayGreen(null);
		setReactionTime(null);
		const timeout = setTimeout(
			() => setTimerDisplayGreen(new Date().getTime()),
			randomNumber(2000, 7000)
		);
		setTimeoutGreen(timeout);
	};

	const stopTimer = useCallback(() => {
		if (timerDisplayGreen) {
			const end = new Date().getTime();
			const timeDiff = end - timerDisplayGreen;
			setReactionTime(timeDiff);
			if (timeDiff < 350 && !isAllowed) {
				setIsAllowed(true);
				raiseValueLoader();
			}
		} else {
			clearTimeout(timeoutGreen);
			setIsToSoon(true);
		}

		setHasStarted(false);
	}, [timerDisplayGreen, timeoutGreen]);

	const statusGame = useMemo(() => {
		if (!hasStarted && !reactionTime && !isTooSoon) return "off";
		else if (reactionTime) return "ended";
		else if (hasStarted && !timerDisplayGreen) return "waiting";
		else if (hasStarted && timerDisplayGreen) return "clickable";
		else if (isTooSoon) return "tooSoon";
	}, [timerDisplayGreen, reactionTime, hasStarted, isTooSoon]);

	return (
		<div className={styles.reactionClickSection}>
			<div className={styles.nextLevel}>
				<Button
					Icon={RightArrowIcon}
					color="primary"
					onClick={() => {
						skipGame();
						nextStepGame();
					}}
				>
					Passer au jeu suivant
				</Button>
			</div>
			<h1 className={`${styles.titleStepGame}`}>Tronc cérébral</h1>
			<div className={styles.reactionClick}>
				<GameIntro>
					<div className={stylesIntro.block}>
						<span className={stylesIntro.label}>Commandes: </span>
						<p className={stylesIntro.value}>Clique gauche</p>
					</div>
					<div className={stylesIntro.block}>
						<div className={stylesIntro.label}>Consigne: </div>
						<div className={stylesIntro.value}>
							<p>
								Lorsque le carré orange passe au vert, clique le plus rapidement
								possible
							</p>
						</div>
					</div>
					<div className={stylesIntro.block}>
						<div className={stylesIntro.label}>Objectif: </div>
						<div className={stylesIntro.value}>
							<p>Moins de 350 ms</p>
						</div>
					</div>
				</GameIntro>

				<div className={styles.reactionClickContent}>
					{!isAllowed && (
						<div
							className={`${styles.reactionClickSquare} ${styles[statusGame]}`}
							onClick={!hasStarted ? launchTimer : stopTimer}
						>
							{statusGame === "off" ? (
								<>
									<FlashIcon style={styles.svgFlash} />
									<p>Cliquez ici pour commencer</p>
								</>
							) : statusGame === "ended" ? (
								<>
									<TimeIcon style={styles.svgFlash} />
									<p className={styles.displayTime}>{`${reactionTime} ms`}</p>
									<p>Cliquez ici pour recommencer</p>
								</>
							) : statusGame !== "tooSoon" ? (
								<>
									<DotsIcon style={styles.svgFlash} />
									<p>
										{statusGame === "waiting" ? "Attendez le vert" : "Click !"}
									</p>
								</>
							) : (
								<>
									<TimeIcon style={styles.svgFlash} />
									<p>Trop tôt !</p>
									<p>Clique si pour recommencer</p>
								</>
							)}
						</div>
					)}
					{isAllowed && statusGame === "ended" && (
						<div className={styles.goNextGame}>
							<p className={styles.displayTime}>{`${reactionTime} ms !`}</p>
							<Button
								Icon={RightArrowIcon}
								color="secondary"
								onClick={nextStepGame}
								width="100%"
							>
								Entrer dans le "Faisceau pyramidal"
							</Button>
						</div>
					)}
				</div>
			</div>
		</div>
	);
}
