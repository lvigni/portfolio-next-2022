import { useCallback, useState } from "react";
import Button from "../../../components/Button/Button";
import PlayIcon from "../../../components/svg/PlayIcon";
import RestartIcon from "../../../components/svg/RestartIcon";
import { randomNumber } from "../../../utils/utils";
import styles from "./SequenceMemory.module.scss";
import stylesIntro from "../../../components/game-intro/GameIntro.module.scss";
import GameIntro from "../../../components/game-intro/GameIntro";
import RightArrowIcon from "../../../components/svg/RightArrowIcon";
import { useRouter } from "next/navigation";

const squares = [
	[0, 0, 0],
	[0, 0, 0],
	[0, 0, 0],
];

const positions = [
	[0, 0],
	[0, 1],
	[0, 2],
	[1, 0],
	[1, 1],
	[1, 2],
	[2, 0],
	[2, 1],
	[2, 2],
];

export default function SequenceMemory({ raiseValueLoader, skipGame }) {
	const [level, setLevel] = useState(0);
	const [isRunning, setIsRunning] = useState(false);
	const [hasStarted, setHasStarted] = useState(false);
	const [currentSquare, setCurrentSquare] = useState();
	const [completeSequence, setCompleteSequence] = useState();
	const [stepResponse, setStepResponse] = useState(0);
	const [isFailed, setIsFailed] = useState(false);
	const [animeSquare, setAnimeSquare] = useState(null);
	const [scaleGame, setScaleGame] = useState(false);
	const router = useRouter();

	const redirectPortfolio = () => router.push("/portfolio");
	function sleep(ms) {
		return new Promise((resolve) => setTimeout(resolve, ms));
	}
	const isSameArrays = (arrayAttempt, arrayResponse) => {
		return JSON.stringify(arrayAttempt) === JSON.stringify(arrayResponse);
	};
	const generatePosSquare = (seq) => {
		const newPositions =
			seq.length > 0
				? positions.filter((pos) => pos !== seq.at(-1))
				: positions;
		const pos = newPositions[randomNumber(0, seq.length > 0 ? 7 : 8)];
		return pos;
	};

	const launchRandomSquares = async (lvl) => {
		setIsRunning(true);
		let newSequence = [];
		if (lvl === 1) {
			for (let index = 1; index <= 3; index++) {
				await sleep(700);
				const randomIndex = generatePosSquare(newSequence);
				setCurrentSquare(randomIndex);
				newSequence.push(randomIndex);
			}
		} else {
			await sleep(1000);
			for (let index = 1; index <= completeSequence?.length; index++) {
				await sleep(700);
				const randomIndex = completeSequence[index - 1];
				setCurrentSquare(randomIndex);
				newSequence.push(randomIndex);
			}
			await sleep(700);
			const randomIndex = generatePosSquare(newSequence);
			setCurrentSquare(randomIndex);
			newSequence.push(randomIndex);
		}

		setTimeout(() => {
			setCompleteSequence(newSequence);
			setCurrentSquare(null);
			setIsRunning(false);
		}, 700);
	};

	const getSquareClass = useCallback(
		(indexRow, index) => {
			const currentRowIndex = currentSquare[0];
			const currentSquareIndex = currentSquare[1];
			return indexRow === currentRowIndex && index === currentSquareIndex
				? "active"
				: "";
		},
		[currentSquare]
	);

	const restartGame = () => {
		setCompleteSequence(null);
		setLevel(0);
		setIsFailed(false);
		setLevel(1);
		launchRandomSquares(1);
	};

	const handleEndGame = async () => {
		setAnimeSquare("end");
		await sleep(1100);
		setScaleGame(true);
		await sleep(1000);
		redirectPortfolio();
	};

	const handleClickSquare = useCallback(
		async (indexRow, index) => {
			if (isSameArrays(completeSequence[stepResponse], [indexRow, index])) {
				setAnimeSquare([indexRow, index]);
				setStepResponse((prec) => prec + 1);
				if (stepResponse + 1 === completeSequence.length) {
					if (level !== 4) {
						setAnimeSquare("all");
						const newLevel = level + 1;
						setLevel(newLevel);
						setStepResponse(0);
						launchRandomSquares(newLevel);
					} else {
						raiseValueLoader();
						handleEndGame();
					}
				}
			} else {
				setIsFailed(true);
				setIsRunning(false);
				setStepResponse(0);
			}
		},
		[completeSequence, stepResponse, level]
	);

	const handleEndAnimation = () => {
		setAnimeSquare(null);
	};

	return (
		<div className={styles.sequenceMemorySection}>
			<div className={styles.nextLevel}>
				<Button
					Icon={RightArrowIcon}
					color="primary"
					onClick={() => {
						skipGame();
						level > 0 ? handleEndGame() : redirectPortfolio();
					}}
				>
					Accéder au portfolio
				</Button>
			</div>
			<h1 className={`${styles.titleStepGame}`}>Cortex visuel</h1>
			<div className={styles.sequenceMemory}>
				<GameIntro>
					<div className={stylesIntro.block}>
						<span className={stylesIntro.label}>Commandes: </span>
						<p className={stylesIntro.value}>Clique gauche</p>
					</div>
					<div className={stylesIntro.block}>
						<span className={stylesIntro.label}>Consigne: </span>
						<div className={stylesIntro.value}>
							<p>- Mémorise la séquence et reproduit la.</p>
						</div>
					</div>
					<div className={stylesIntro.block}>
						<span className={stylesIntro.label}>Objectif: </span>
						<div className={stylesIntro.value}>
							<p>7 d'affilés</p>
						</div>
					</div>
				</GameIntro>
				<div className={styles.generationContent}>
					{!hasStarted && !isRunning && !isFailed && level !== 2 ? (
						<Button
							Icon={PlayIcon}
							color="secondary"
							onClick={() => {
								const newLevel = level + 1;
								setLevel(newLevel);
								setHasStarted(true);
								launchRandomSquares(newLevel);
							}}
						>
							Commencer
						</Button>
					) : hasStarted && !isFailed ? (
						<div
							className={`${styles.squares}	${
								animeSquare === "end" ? styles.flashEnd : ""
							} ${scaleGame ? styles.scale : ""}`}
						>
							{squares.map((squareRow, indexRow) => (
								<div
									className={styles.squareRow}
									key={`square-row-generated-${indexRow}}`}
								>
									{squareRow.map((square, index) => (
										<div
											onClick={() =>
												!isRunning && handleClickSquare(indexRow, index)
											}
											onAnimationEnd={handleEndAnimation}
											className={`${styles.square} ${
												currentSquare?.length
													? styles[getSquareClass(indexRow, index)]
													: ""
											} ${
												isSameArrays(animeSquare, [indexRow, index])
													? styles.flashOnce
													: ""
											} ${animeSquare === "all" ? styles.flashTwice : ""}
											${animeSquare === "end" ? styles.flashEnd : ""}`}
											key={`square-generated-${index}}`}
										></div>
									))}
								</div>
							))}
						</div>
					) : isFailed && !isRunning ? (
						<div className={styles.isFailed}>
							<span className={styles.title}>Dommage..</span>
							<Button
								Icon={RestartIcon}
								color="secondary"
								onClick={restartGame}
								width="100%"
							>
								Recommencer
							</Button>
						</div>
					) : (
						<>
							<Button
								Icon={RestartIcon}
								color="secondary"
								onClick={restartGame}
								width="100%"
							>
								Voir le portfolio
							</Button>
						</>
					)}
				</div>
			</div>
		</div>
	);
}
