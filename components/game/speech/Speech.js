import Link from "next/link";
import { useCallback } from "react";
import Button from "../../../components/Button/Button";
import NextIcon from "../../../components/svg/NextIcon";
import PlayIcon from "../../../components/svg/PlayIcon";
import PreviousIcon from "../../../components/svg/PreviousIcon";
import RightArrowIcon from "../../../components/svg/RightArrowIcon";
import styles from "./Speech.module.scss";

function Speech({ stepShowSpeech, nextStep, previousStep, onClickPlay }) {
	const displayNext = useCallback(() => {
		return ![0, 1, 8].includes(stepShowSpeech);
	}, [stepShowSpeech]);
	const displayPrevious = useCallback(() => {
		return ![0, 1, 2, 8].includes(stepShowSpeech);
	}, [stepShowSpeech]);

	return (
		<div className={styles.speechGameSection}>
			<div
				className={`${styles.navigateSection} ${styles.next} ${
					displayNext() ? styles.show : ""
				}`}
				onClick={() => nextStep()}
			>
				<NextIcon style={styles.nextSpeech} />
			</div>
			<div
				className={`${styles.navigateSection} ${styles.previous} ${
					displayPrevious() ? styles.show : ""
				}`}
				onClick={() => previousStep()}
			>
				<PreviousIcon style={styles.nextSpeech} />
			</div>
			<div
				className={`${styles.paragraph} ${styles.speechBlock} ${
					stepShowSpeech === 2 ? styles.show : ""
				}`}
			>
				<p>Bonjour utilisateur,</p>
				<p>
					Il ne s'agit pas d'une erreur dans le code de mon créateur, mais d'une
					nouvelle expérience de simulation.
				</p>
			</div>
			<div
				className={`${styles.paragraph} ${styles.speechBlock} ${
					stepShowSpeech === 3 ? styles.show : ""
				}`}
			>
				<p>Bienvenue dans la matrice V1-GN1.</p>
				<p>
					C'est ici, dans le centre névralgique de mon créateur, que sont
					conçues mes lignes de code. Tu cherches probablement à en savoir plus
					sur lui.
				</p>
			</div>
			<div
				className={`${styles.paragraph} ${styles.speechBlock} ${
					stepShowSpeech === 4 ? styles.show : ""
				}`}
			>
				<p>Cela ne sera possible que lorsque tu atteindras les 100%.</p>
				<p>
					Cette barre ne représente pas un simple chargement de page, mais elle
					représente, ta progression vers son centre névralgique.
				</p>
			</div>
			<div
				className={`${styles.paragraph} ${styles.speechBlock} ${
					stepShowSpeech === 5 ? styles.show : ""
				}`}
			>
				<p>
					Malheureusement utilisateur, comme tu peux le constater, tu es loin
					des 100%.
				</p>
				<p>
					Mais ne t'en fais pas, tu peux augmenter ce pourcentage en travaillant
					des parties du cerveau de mon créateur.
				</p>
			</div>
			<div
				className={`${styles.paragraph} ${styles.speechBlock} ${
					stepShowSpeech === 6 ? styles.show : ""
				}`}
			>
				<p>
					J'aimerais beaucoup t'aider dans tes tâches, mais figures toi que je
					n'ai pas été programmé pour, c'est donc à toi et toi seul,
					utilisateur, qu'est incombé cette tâche.
				</p>
			</div>
			<div
				className={`${styles.paragraph} ${styles.speechBlock} ${
					stepShowSpeech === 7 ? styles.show : ""
				}`}
			>
				<p>
					Sur ce, je te laisse, je dois retrouver cet utilisateur bloqué dans la
					simulation depuis le mois dernier..
				</p>
			</div>
			<div
				className={`${styles.buttons} ${styles.speechBlock} ${
					stepShowSpeech === 8 ? styles.show : ""
				}`}
			>
				<Button
					Icon={PlayIcon}
					color="secondary"
					onClick={onClickPlay}
					disabled={stepShowSpeech !== 8}
				>
					Réveiller ses neuronnes
				</Button>
				<Button
					Icon={RightArrowIcon}
					color="primary"
					disabled={stepShowSpeech !== 8}
				>
					<Link href="/portfolio">Ignorer l'immersion</Link>
				</Button>
			</div>
		</div>
	);
}

export default Speech;
