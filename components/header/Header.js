import styles from "./Header.module.scss";

export default function Header() {
	return (
		<div className={styles.header}>
			<div className={styles.leftSide}>
				<span>Lv</span>
			</div>
			<div className={styles.rightSide}>
				<span>Présentation</span>
				<span>Parcours</span>
				<span>Compétences</span>
				<span>Réalisations</span>
				<span>Contact</span>
			</div>
		</div>
	);
}
