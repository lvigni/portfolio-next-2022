import ImageLink from "./image-link/ImageLink";
import styles from "./ImageLabel.module.scss";

function ImageLabel({ path, label, width, ...props }) {
	return (
		<div className={styles.imageLabel} style={{ width }}>
			<ImageLink path={path} {...props} />
			{label && <span className={styles.label}>{label}</span>}
		</div>
	);
}

export default ImageLabel;
