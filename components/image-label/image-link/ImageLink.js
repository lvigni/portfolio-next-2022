import styles from "./ImageLink.module.scss";
function ImageLink({ path, width = "100%", link, ...props }) {
	const navigate = () => {
		link && window.open(link, "_blank").focus();
	};
	return (
		<div
			className={`${styles.imageContainer} ${link ? styles.hasLink : ""}`}
			styles={{ width }}
		>
			<img src={path} {...props} onClick={navigate} alt="" />
		</div>
	);
}

export default ImageLink;
