import styles from "./Modal.module.scss";
import { motion } from "framer-motion";
import CrossIcon from "../svg/CrossIcon";
import { Carousel } from "flowbite-react";
import { screenSuperiorOf } from "../../utils/utils";

function Modal({ project, onClose }) {
	return (
		<>
			<motion.div
				className={styles.bgModal}
				onClick={onClose}
				initial={{ opacity: 0 }}
				animate={{ opacity: 1 }}
				exit={{ opacity: 0 }}
				transition={{ delay: 0.1 }}
			></motion.div>
			<motion.div
				layoutId={project.id}
				initial={{ y: 0 }}
				animate={{ y: -25 }}
				transition={{ delay: 0.1 }}
				className={styles.modalContainer}
			>
				<div className={styles.modal}>
					<h3>{project.title}</h3>
					<CrossIcon className={styles.close} onClick={onClose} />
					<div className={styles.description}>{project.description}</div>
					{screenSuperiorOf(768) && (
						<div className={styles.carousel}>
							<Carousel slide={false}>
								{project.pictures.map((url) => (
									<img src={url} alt="" />
								))}
							</Carousel>
						</div>
					)}
					<div className={styles.bottomInfos}>
						<div className={styles.features}>
							<h4>Quelques features</h4>
							<ul>
								{project.features.map((feature, i) => (
									<li key={"project-feature" + i}>{feature}</li>
								))}
							</ul>
						</div>
						<div className={styles.technos}>
							{project.technos.map((technoUrl) => (
								<img key={technoUrl} src={technoUrl} alt="test" />
							))}
						</div>
					</div>
				</div>
			</motion.div>
		</>
	);
}

export default Modal;
