import { useState } from "react";
import AccordionCard from "../../../components/accordion-card/AccordionCard";
import JobIcon from "../../../components/svg/JobIcon";
import LocalisationIcon from "../../../components/svg/LocalisationIcon";
import SchoolIcon from "../../../components/svg/SchoolIcon";
import TitlePortfolio from "../../../components/title-portfolio/TitlePortfolio";
import {
	airFranceTags,
	avrTags,
	btsTags,
	esgiTags,
	lenormantTags,
	licenceTags,
	masterTags,
	oxyleadTags,
	webexprTags,
} from "../../../utils/parcours-tags";
import { screenSuperiorOf } from "../../../utils/utils";
import styles from "./History.module.scss";

function History() {
	const [isOpenFormation, setIsOpenFormation] = useState();
	const [isOpenJob, setIsOpenJob] = useState();

	return (
		<div className={styles.contentHistory}>
			<TitlePortfolio
				title="Historique"
				subTitle="Mon parcours"
				mb={screenSuperiorOf(768) ? "mb-28" : "mb-14"}
			/>
			<div className={styles.content}>
				<div className={styles.section}>
					<div className={`${styles.icon} ${styles.job}`}>
						<SchoolIcon styles={styles.svg} />
					</div>
					<div className={styles.cards}>
						<AccordionCard
							title="Maitrise - Ingénierie du web"
							duration="2 ans"
							years="2020-2022"
							setIsOpen={() =>
								setIsOpenFormation(isOpenFormation !== "esgi" ? "esgi" : null)
							}
							isOpen={isOpenFormation === "esgi"}
						>
							<div className={styles.contentCard}>
								<div className={styles.localisation}>
									<LocalisationIcon style={styles.localisationSvg} />
									<span>ESGI - Paris</span>
								</div>
								<p className={styles.presentation}>
									J'ai réalisé durant cette formation plus de 20 projets
									différents abordant différentes thématiques sur plusieurs
									technologies. Ces 2 ans m'ont apporté une logique de
									développement et un savoir faire complet.
								</p>
								<div className={styles.tags}>
									{esgiTags.map((tag, i) => (
										<div key={`tag-esgi-${tag}-${i}`} className={styles.tag}>
											<span>{tag}</span>
										</div>
									))}
								</div>
							</div>
						</AccordionCard>
						<AccordionCard
							title="Maitrise - Général informatique"
							duration="1 an"
							years="2019-2020"
							setIsOpen={() =>
								setIsOpenFormation(
									isOpenFormation !== "master" ? "master" : null
								)
							}
							isOpen={isOpenFormation === "master"}
						>
							<div className={styles.contentCard}>
								<div className={styles.localisation}>
									<LocalisationIcon style={styles.localisationSvg} />
									<span>CNAM - Senlis</span>
								</div>
								<p className={styles.presentation}>
									Ici, j'ai ajouté des cordes à mon arc et perfectionner mes
									connaissances. Les tests et la qualité de code ont ajouté une
									grosse plus value à mon travail.
								</p>
								<div className={styles.tags}>
									{masterTags.map((tag, i) => (
										<div
											key={`tag-cnam-master-${tag}-${i}`}
											className={styles.tag}
										>
											<span>{tag}</span>
										</div>
									))}
								</div>
							</div>
						</AccordionCard>
						<AccordionCard
							title="Baccalaureat - Général informatique"
							duration="1 an"
							years="2018-2019"
							setIsOpen={() =>
								setIsOpenFormation(
									isOpenFormation !== "licence" ? "licence" : null
								)
							}
							isOpen={isOpenFormation === "licence"}
						>
							<div className={styles.contentCard}>
								<div className={styles.localisation}>
									<LocalisationIcon style={styles.localisationSvg} />
									<span>CNAM - Senlis</span>
								</div>
								<p className={styles.presentation}>
									De la conception d'un cahier des charges, de la gestion de
									projet, en passant par le développement jusqu'au recettage,
									j'ai développé une vision globale du développement web.
								</p>
								<div className={styles.tags}>
									{licenceTags.map((tag, i) => (
										<div
											key={`tag-cnam-licence-${tag}-${i}`}
											className={styles.tag}
										>
											<span>{tag}</span>
										</div>
									))}
								</div>
							</div>
						</AccordionCard>
						<AccordionCard
							title="Baccalaureat - SIO"
							duration="2 ans"
							years="2016-2018"
							setIsOpen={() =>
								setIsOpenFormation(isOpenFormation !== "bts" ? "bts" : null)
							}
							isOpen={isOpenFormation === "bts"}
						>
							<div className={styles.contentCard}>
								<div className={styles.localisation}>
									<LocalisationIcon style={styles.localisationSvg} />
									<span>Lycée Jean Rostand - Chantilly</span>
								</div>
								<p className={styles.presentation}>
									J'ai appris les bases du développement, et même de
									l'informatique. Algorithme, paradigme de programmation objet,
									SQL, etc. J'ai débloqué toutes les armes pour me lancer dans
									le développement web.
								</p>
								<div className={styles.tags}>
									{btsTags.map((tag, i) => (
										<div key={`tag-bts-${tag}-${i}`} className={styles.tag}>
											<span>{tag}</span>
										</div>
									))}
								</div>
							</div>
						</AccordionCard>
						<AccordionCard
							title="DEC - STI2D"
							duration="1 an"
							years="2015-2016"
							setIsOpen={() =>
								setIsOpenFormation(isOpenFormation !== "bac" ? "bac" : null)
							}
							isOpen={isOpenFormation === "bac"}
						>
							<div className={styles.contentCard}>
								<div className={styles.localisation}>
									<LocalisationIcon style={styles.localisationSvg} />
									<span>Lycée Marie Curie - Nogent sur l'oise</span>
								</div>
								<p className={styles.presentation}>
									Des études basées sur la technologie et l'environnement, tout
									en gardant les matières générales, j'ai développé une
									autonomie et un sens du travail. C'est parti pour les études
									supérieures !
								</p>
							</div>
						</AccordionCard>
					</div>
				</div>
				<div className={styles.section}>
					<div className={`${styles.icon} ${styles.job}`}>
						<JobIcon styles={styles.svg} />
					</div>
					<div className={styles.cards}>
						<AccordionCard
							title="Développeur web - WebexpR"
							duration="2.5 ans"
							years="2020-2023"
							setIsOpen={() =>
								setIsOpenJob(isOpenJob !== "webexpr" ? "webexpr" : null)
							}
							isOpen={isOpenJob === "webexpr"}
						>
							<div className={styles.contentCard}>
								<div className={styles.localisation}>
									<LocalisationIcon style={styles.localisationSvg} />
									<span>WebexpR - Compiègne</span>
								</div>
								<p className={styles.presentation}>
									J'ai travaillé sur des applications web & mobile au sein d'une
									équipe avec une méthodologie SCRUM. Développant sur une stack
									principalement JS/TS, j'ai également travaillé sur d'autres
									technologies au besoin. J'ai eu la chance de former les
									nouveaux collaborateurs, faire de la gestion de projet, des
									réunions clients ainsi que de la configuration serveur.
								</p>
								<div className={styles.tags}>
									{webexprTags.map((tag, i) => (
										<div key={`tag-webexpr-${tag}-${i}`} className={styles.tag}>
											<span>{tag}</span>
										</div>
									))}
								</div>
							</div>
						</AccordionCard>
						<AccordionCard
							title="Développeur - Air france"
							duration="6 mois"
							years="2019-2020"
							setIsOpen={() =>
								setIsOpenJob(isOpenJob !== "airfrance" ? "airfrance" : null)
							}
							isOpen={isOpenJob === "airfrance"}
						>
							<div className={styles.contentCard}>
								<div className={styles.localisation}>
									<LocalisationIcon style={styles.localisationSvg} />
									<span>Air France - Charles de gaulle</span>
								</div>
								<p className={styles.presentation}>
									Au sein de l'équipe d'Air France, mon objectif principal était
									de proposer des améliorations significatives de l'existant,
									tout en élaborant des solutions techniques pour répondre au
									mieux aux besoins internes. Par ailleurs, j'ai été sollicité
									pour apporter mon soutien technique aux développeurs
									travaillant sur des projets aussi bien web qu'applicatif.
								</p>
								<div className={styles.tags}>
									{airFranceTags.map((tag, i) => (
										<div
											key={`tag-airfrance-${tag}-${i}`}
											className={styles.tag}
										>
											<span>{tag}</span>
										</div>
									))}
								</div>
							</div>
						</AccordionCard>
						<AccordionCard
							title="Dévelopeur - Oxylead"
							duration="6 mois"
							years="2018-2019"
							setIsOpen={() =>
								setIsOpenJob(isOpenJob !== "oxylead" ? "oxylead" : null)
							}
							isOpen={isOpenJob === "oxylead"}
						>
							<div className={styles.contentCard}>
								<div className={styles.localisation}>
									<LocalisationIcon style={styles.localisationSvg} />
									<span>Oxylead - Paris</span>
								</div>
								<p className={styles.presentation}>
									J'ai été amené à travailler en étroite collaboration avec des
									clients disposant tous d'une application personnalisée de
									Microsoft 365. Mon rôle était de m'occuper de la Tierce
									Maintenance Applicative (TMA) et de la Tierce Maintenance
									Évolutive (TME) de ces applications, ainsi que de traiter les
									incidents urgents qui pouvaient survenir.
								</p>
								<div className={styles.tags}>
									{oxyleadTags.map((tag, i) => (
										<div key={`tag-oxylead-${tag}-${i}`} className={styles.tag}>
											<span>{tag}</span>
										</div>
									))}
								</div>
							</div>
						</AccordionCard>
						<AccordionCard
							title="Testeur front - AVR Solutions"
							duration="2 mois"
							years="2018"
							setIsOpen={() => setIsOpenJob(isOpenJob !== "avr" ? "avr" : null)}
							isOpen={isOpenJob === "avr"}
						>
							<div className={styles.contentCard}>
								<div className={styles.localisation}>
									<LocalisationIcon style={styles.localisationSvg} />
									<span>AVR Solutions - Paris</span>
								</div>
								<p className={styles.presentation}>
									Au sein de l'équipe chargée du développement d'une
									application, j'ai été en charge de la réalisation de tests
									front-end pour garantir une couverture de test maximale. Mon
									objectif était de m'assurer que tous les scénarios
									d'utilisation étaient pris en compte, afin d'identifier les
									éventuels bogues et de les corriger rapidement.
								</p>
								<div className={styles.tags}>
									{avrTags.map((tag, i) => (
										<div
											key={`tag-avr-solutions-${tag}-${i}`}
											className={styles.tag}
										>
											<span>{tag}</span>
										</div>
									))}
								</div>
							</div>
						</AccordionCard>
						<AccordionCard
							title="Dévelopeur web - Lenormant"
							duration="1 mois"
							years="2017"
							setIsOpen={() =>
								setIsOpenJob(isOpenJob !== "lenormant" ? "lenormant" : null)
							}
							isOpen={isOpenJob === "lenormant"}
						>
							<div className={styles.contentCard}>
								<div className={styles.localisation}>
									<LocalisationIcon style={styles.localisationSvg} />
									<span>Lenormant - Nogent sur Oise</span>
								</div>
								<p className={styles.presentation}>
									Hardware, software, développement web, j'ai pu apprendre plein
									de bases utile pour la suite de mon parcours.
								</p>
								<div className={styles.tags}>
									{lenormantTags.map((tag, i) => (
										<div
											key={`tag-lenormant-${tag}-${i}`}
											className={styles.tag}
										>
											<span>{tag}</span>
										</div>
									))}
								</div>
							</div>
						</AccordionCard>
					</div>
				</div>
			</div>
		</div>
	);
}

export default History;
