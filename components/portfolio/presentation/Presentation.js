import styles from "./Presentation.module.scss";
import TitlePortfolio from "../../../components/title-portfolio/TitlePortfolio";
import Button from "../../../components/Button/Button";
import DownloadIcon from "../../../components/svg/DownloadIcon";
import { useState } from "react";
import { screenSuperiorOf } from "../../../utils/utils";

function Presentation() {
	const [mailIsCopy, setMailIsCopy] = useState(false);
	return (
		<div className={styles.contentCell}>
			<div className={`${styles.textCopy} ${mailIsCopy ? styles.show : ""}`}>
				<span>Vous avez copié le mail</span>
			</div>
			<TitlePortfolio
				title="Présentation"
				subTitle="Qui suis-je ?"
				mb={screenSuperiorOf(768) ? "mb-32" : "mb-16"}
			/>
			<div className={styles.content}>
				<div className={styles.pictureContainer}>
					<img src="/neon-picture.png" alt="" />
				</div>
				<div className={styles.aboutContainer}>
					<p>
						<span>Développeur</span> depuis 2016, je me suis spécialisé au fur
						et à mesure dans le <span>web</span> et <span>mobile</span>.
					</p>
					<p>
						Durant mon parcours, j'ai acquis une{" "}
						<span>expertise technique</span> m'offrant une{" "}
						<span>polyvalence</span> sur de nombreuses stacks. Au-delà de la
						technique, mon expérience m'a permis de développer une{" "}
						<span>réflexion approfondie</span> sur mon métier me permettant de{" "}
						<span>m'adapter</span> facilement à différents sujets.
					</p>
					<p>
						Ma <span>détermination</span> est nourrie par mon envie de
						progresser <span>humainement</span> et <span>techniquement</span> en
						partageant des expériences riches.
					</p>
					<p>
						Étant passionné par mon travail, <span>j'aime apprendre</span> en{" "}
						continu pour rester à jour avec les dernières tendances et{" "}
						technologies.
					</p>
					<p className={styles.citation}>
						Je ne peux pas être épanoui dans un travail ou je n'apprends plus
						rien. Ça tombe bien.. Je suis développeur web !
					</p>
					<div className={styles.mediasContainer}>
						<Button
							Icon={DownloadIcon}
							color="secondary"
							onClick={() => window.open("/cv.pdf")}
						>
							Téléchargez mon CV
						</Button>
						<div className={styles.pictos}>
							<img
								src="/linkedin.png"
								alt=""
								onClick={() =>
									window.open(
										"https://www.linkedin.com/in/lucas-vigni-62704216a"
									)
								}
								className={styles.linkedin}
							/>
							<div className={styles.mailSection}>
								<img
									src="/mail.png"
									alt=""
									onClick={() => {
										setMailIsCopy(true);
										navigator.clipboard.writeText("vigni.pro@gmail.com");
										setTimeout(() => setMailIsCopy(false), 4000);
									}}
									className={styles.mail}
								/>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	);
}

export default Presentation;
