import styles from "./Projects.module.scss";
import TitlePortfolio from "../../../components/title-portfolio/TitlePortfolio";
import { useState } from "react";
import { AnimatePresence, motion } from "framer-motion";
import projectItems from "../../../utils/project-items";
import Modal from "../../../components/modal/Modal";
import { screenSuperiorOf } from "../../../utils/utils";

export default function Projects() {
	const [section, setSection] = useState();
	const [selected, setSelected] = useState(null);

	const getIsActive = (sec) => {
		return section === sec ? styles.active : "";
	};

	return (
		<div className={styles.contentProjects}>
			<TitlePortfolio
				title="Quelques projets"
				subTitle="Mes travaux"
				mb={screenSuperiorOf(768) ? "mb-28" : "mb-14"}
			/>

			<div className={styles.projects}>
				<div className={styles.cards}>
					{projectItems.map((item) => (
						<motion.div
							layoutId={item.id}
							onClick={() => {
								setSelected(item);
								if (typeof window !== "undefined") window.scrollTo(0, 0);
							}}
							className={`${styles.card} ${styles.cardBase}`}
						>
							<h2>{item.title}</h2>
							<h5>{item.subtitle}</h5>
							<div className={styles.footer}>
								<div className="flex justify-end">
									<LabelCard type={item.label} />
								</div>
							</div>
						</motion.div>
					))}
				</div>

				{selected && (
					<AnimatePresence>
						<Modal project={selected} onClose={() => setSelected(null)} />
					</AnimatePresence>
				)}
			</div>
		</div>
	);
}

const LabelCard = ({ type = "pro" }) => {
	return (
		<div className={`${styles.containerLabel}`}>
			<span className={styles.label}>
				{type === "pro" ? "Professionel" : "Personnel"}
			</span>
		</div>
	);
};
