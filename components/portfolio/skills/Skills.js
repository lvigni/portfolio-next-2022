import styles from "./Skills.module.scss";
import TitlePortfolio from "../../../components/title-portfolio/TitlePortfolio";
import FrameworkIcon from "../../../components/svg/FrameworkIcon";
import DatabaseIcon from "../../../components/svg/DatabaseIcon";
import TestIcon from "../../../components/svg/TestIcon";
import PlusIcon from "../../../components/svg/PlusIcon";
import FolderIcon from "../../../components/svg/FolderIcon";
import { useEffect, useState } from "react";
import { motion } from "framer-motion";
import {
	bddMedias,
	frameworkMedias,
	plusMedias,
	testMedias,
	versioningMedias,
	languagesMedias,
} from "../../../utils/skills-images";
import CodeIcon from "../../../components/svg/CodeIcon";
import { screenSuperiorOf } from "../../../utils/utils";

export default function Skills() {
	const [section, setSection] = useState("framework");
	const [domLoaded, setDomLoaded] = useState(false);

	useEffect(() => {
		setDomLoaded(true);
	}, []);

	const getIsActive = (sec) => {
		return section === sec ? styles.active : "";
	};

	const getSectionSkills = () => {
		let medias;
		switch (section) {
			case "framework":
				medias = <FrameworkSection />;
				break;
			case "languages":
				medias = <LanguagesSection />;
				break;
			case "bdd":
				medias = <DatabaseSection />;
				break;
			case "test":
				medias = <TestSection />;
				break;
			case "versioning":
				medias = <VersioningSection />;
				break;
			case "plus":
				medias = <PlusSection />;
				break;
		}

		return (
			<div className={styles.contentSkill}>
				<div className={styles.framework}>
					<div className={styles.frameworkMedias}>{medias}</div>
				</div>
			</div>
		);
	};

	return (
		<div className={styles.contentCell}>
			<TitlePortfolio
				title="Compétences"
				subTitle="Savoir-faire"
				mb={screenSuperiorOf(768) ? "mb-28" : "mb-14"}
			/>
			{domLoaded && (
				<div className={styles.containerSkills}>
					<div className={styles.topMenu}>
						<div
							className={`${styles.skillCard} ${getIsActive("framework")}`}
							onClick={() => setSection("framework")}
						>
							<FrameworkIcon style={styles.svg} />
							<h3>Framework</h3>
						</div>
						<div
							className={`${styles.skillCard} ${getIsActive("languages")}`}
							onClick={() => setSection("languages")}
						>
							<CodeIcon style={styles.svg} />
							<h3>Langages</h3>
						</div>
						<div
							className={`${styles.skillCard} ${getIsActive("bdd")}`}
							onClick={() => setSection("bdd")}
						>
							<DatabaseIcon style={styles.svg} />
							<h3>Base de données</h3>
						</div>
						<div
							className={`${styles.skillCard} ${getIsActive("test")}`}
							onClick={() => setSection("test")}
						>
							<TestIcon style={styles.svg} />
							<h3>Testing</h3>
						</div>
						<div
							className={`${styles.skillCard} ${getIsActive("versioning")}`}
							onClick={() => setSection("versioning")}
						>
							<FolderIcon style={styles.svg} />
							<h3>Versioning</h3>
						</div>
						<div
							className={`${styles.skillCard} ${getIsActive("plus")}`}
							onClick={() => setSection("plus")}
						>
							<PlusIcon style={styles.svg} />
							<h3>Plus</h3>
						</div>
					</div>
					{getSectionSkills()}
				</div>
			)}
		</div>
	);
}

const FrameworkSection = () => {
	return frameworkMedias.map((elem, i) => (
		<motion.div
			key={`framework-${i}`}
			initial={{ y: 10, opacity: 0 }}
			animate={{ y: 0, opacity: 1 }}
			exit={{ y: -10, opacity: 0 }}
			transition={{ delay: 0.1 * i }}
		>
			{elem}
		</motion.div>
	));
};

const LanguagesSection = () => {
	return languagesMedias.map((elem, i) => (
		<motion.div
			key={`languages-${i}`}
			initial={{ y: 10, opacity: 0 }}
			animate={{ y: 0, opacity: 1 }}
			exit={{ y: -10, opacity: 0 }}
			transition={{ delay: 0.1 * i }}
		>
			{elem}
		</motion.div>
	));
};

const DatabaseSection = () => {
	return bddMedias.map((elem, i) => (
		<motion.div
			className={screenSuperiorOf("480") ? "mt-12" : "mt-0"}
			key={`bdd-${i}`}
			initial={{ y: 10, opacity: 0 }}
			animate={{ y: 0, opacity: 1 }}
			exit={{ y: -10, opacity: 0 }}
			transition={{ delay: 0.1 * i }}
		>
			{elem}
		</motion.div>
	));
};

const TestSection = () => {
	return testMedias.map((elem, i) => (
		<motion.div
			className="mt-12"
			key={`test-${i}`}
			initial={{ y: 10, opacity: 0 }}
			animate={{ y: 0, opacity: 1 }}
			exit={{ y: -10, opacity: 0 }}
			transition={{ delay: 0.1 * i }}
		>
			{elem}
		</motion.div>
	));
};

const VersioningSection = () => {
	return versioningMedias.map((elem, i) => (
		<motion.div
			className="mt-12"
			key={`versionning-${i}`}
			initial={{ y: 10, opacity: 0 }}
			animate={{ y: 0, opacity: 1 }}
			exit={{ y: -10, opacity: 0 }}
			transition={{ delay: 0.1 * i }}
		>
			{elem}
		</motion.div>
	));
};

const PlusSection = () => {
	return plusMedias.map((elem, i) => (
		<motion.div
			className="mt-12"
			key={`plus-${i}`}
			initial={{ y: 10, opacity: 0 }}
			animate={{ y: 0, opacity: 1 }}
			exit={{ y: -10, opacity: 0 }}
			transition={{ delay: 0.1 * i }}
		>
			{elem}
		</motion.div>
	));
};
