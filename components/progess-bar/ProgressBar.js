import { useEffect, useState } from "react";
import { randomNumber } from "../../utils/utils";
import styles from "./ProgressBar.module.scss";

function ProgressBar({ stepSpeech, valueLoader, setValueLoader, resetValue }) {
	useEffect(() => {
		let value = 0;
		let index = 0;

		const interval = setInterval(() => {
			if (index === 40) {
				setValueLoader(0);
				clearInterval(interval);
			} else if (value !== 100) {
				const newValue = calculValueProgressBar(index);
				setValueLoader(newValue);
				value = newValue;
				index++;
			} else clearInterval(interval);
		}, 1000);
		if (resetValue) {
			clearInterval(interval);
			return;
		}
		return () => {
			clearInterval(interval);
		};
	}, [resetValue]);

	const calculValueProgressBar = (index) => {
		switch (index) {
			case 0:
				return 11;
			case 1:
				return 35;
			case 2:
				return 54;
			case 3:
				return 99;
			default:
				return randomNumber(0, 100);
		}
	};

	return (
		<div className={styles.loaderSection}>
			<div className={styles.progressBarSection}>
				<div className={styles.infosProgressBar}>
					{stepSpeech === 0 && (
						<div className={styles.loading}>
							<div className={styles.loadingText}>
								<span className={styles.loadingTextWords}>P</span>
								<span className={styles.loadingTextWords}>a</span>
								<span className={styles.loadingTextWords}>t</span>
								<span className={styles.loadingTextWords}>i</span>
								<span className={styles.loadingTextWords}>e</span>
								<span className={styles.loadingTextWords}>n</span>
								<span className={styles.loadingTextWords}>t</span>
								<span className={styles.loadingTextWords}>e</span>
								<span className={styles.loadingTextWords}>z</span>
								<span className={styles.loadingTextWords}>.</span>
								<span className={styles.loadingTextWords}>.</span>
							</div>
						</div>
					)}
					{stepSpeech === 1 && <span className={styles.error}>Erreur.</span>}
					{stepSpeech > 1 && <div></div>}
					<span className={styles.valueLoader}>{`${valueLoader}%`}</span>
				</div>
				<div className="w-full bg-gray-200 rounded-full h-2.5 dark:bg-gray-700">
					<div
						className={styles.loaderValue}
						style={{ width: `${valueLoader}%` }}
					></div>
				</div>
			</div>
		</div>
	);
}

export default ProgressBar;
