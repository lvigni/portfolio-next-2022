import styles from "./SectionPortfolio.module.scss";

export default function SectionPortfolio({ children, hasBgDark = true }) {
	return (
		<div
			className={`${styles.sectionPortfolio} ${
				hasBgDark ? styles.dark : styles.light
			}`}
		>
			{children}
		</div>
	);
}
