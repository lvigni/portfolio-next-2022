import styles from "./SkillCard.module.scss";

export default function SkillCard({ children, title, Icon }) {
	return (
		<div className={styles.block}>
			<div className={styles.cover}>
				<span className={styles.title}>{title}</span>
				<div className={styles.icon}>
					<Icon style={styles.svg} />
				</div>
			</div>
			<div className={styles.details}>{children}</div>
		</div>
	);
}
