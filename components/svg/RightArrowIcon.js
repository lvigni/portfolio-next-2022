export default function RightArrowIcon({ style }) {
	return (
		<svg
			fill="none"
			viewBox="0 0 24 24"
			strokeWidth="1.5"
			stroke="currentColor"
			className={style}
		>
			<path
				strokeLinecap="round"
				strokeLinejoin="round"
				d="M4.5 12h15m0 0l-6.75-6.75M19.5 12l-6.75 6.75"
			/>
		</svg>
	);
}
