import styles from "./TitlePortfolio.module.scss";

function TitlePortfolio({ title, subTitle, mb = "mb-40" }) {
	return (
		<div className={`${styles.title} ${mb}`}>
			<h2>
				{title} <span>{subTitle}</span>
			</h2>
			<hr />
		</div>
	);
}

export default TitlePortfolio;
