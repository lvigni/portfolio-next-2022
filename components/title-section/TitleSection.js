import styles from "./TitleSection.module.scss";

export default function TitleSection({ title, titleBehind, left = "15%" }) {
	return (
		<div className={styles.titleSection}>
			<h2 className={styles.title}>
				{title}
				<span className={styles.titleBehind} style={{ left }}>
					{titleBehind}
				</span>
			</h2>
			<hr />
		</div>
	);
}
