import { use, useEffect, useState } from "react";
import ParticlesArea from "../components/ParticlesArea/ParticlesArea";
import Speech from "../components/game/speech/Speech";
import styles from "./game.module.scss";
import MemoryNumber from "../components/game/memory-number/MemoryNumber";
import ReactionClick from "../components/game/reaction-click/ReactionClick";
import CognitiveTest from "../components/game/cognitive-test/CognitiveTest";
import SequenceMemory from "../components/game/sequence-memory/SequenceMemory";
import ProgressBar from "../components/progess-bar/ProgressBar";
import { motion } from "framer-motion";
import Body from "../components/body/Body";
import Link from "next/link";
import { useRouter } from "next/navigation";
import RightArrowIcon from "../components/svg/RightArrowIcon";
import Button from "../components/Button/Button";
import FrameworkIcon from "../components/svg/FrameworkIcon";
import { screenSuperiorOf } from "../utils/utils";

function Game({ setShowPortfolio }) {
	const [stepShowSpeech, setStepShowSpeech] = useState(0);
	const [stepGame, setStepGame] = useState(0);
	const [valueLoader, setValueLoader] = useState(0);
	const router = useRouter();
	const [domLoaded, setDomLoaded] = useState(false);

	useEffect(() => {
		setDomLoaded(true);
	}, []);

	useEffect(() => {
		let timer = 0;
		let interval;
		interval = setInterval(() => {
			if (timer === 11) clearInterval(interval);
			timer++;
			const stepValue = getStepAccordingToTimer(timer);
			stepValue && setStepShowSpeech(stepValue);
		}, 1000);
		return () => {
			clearInterval(interval);
		};
	}, []);

	const getStepAccordingToTimer = (timer) => {
		let value;
		switch (timer) {
			case 5:
				value = 1;
				break;
			case 7:
				value = 2;
				break;
		}
		return value;
	};

	useEffect(() => {
		if (typeof window !== "undefined" && window.innerWidth <= 768)
			router.push("/portfolio");
	}, []);
	return domLoaded ? (
		<Body>
			{screenSuperiorOf(1050) ? (
				<div className={styles.loadingPageSection}>
					<span className={styles.skipGame}>
						<Link href="/portfolio">Ignorer l'immersion {">"}</Link>
					</span>
					<div>
						<ParticlesArea />
						<div className={styles.loadingPage}>
							{stepGame === 0 && (
								<>
									<h1
										className={`${styles.titleGame} ${
											stepShowSpeech !== 0 ? styles.show : ""
										}`}
									>
										Mind Game
									</h1>
									<Speech
										stepShowSpeech={stepShowSpeech}
										nextStep={() => {
											!(stepShowSpeech >= 8) &&
												setStepShowSpeech(stepShowSpeech + 1);
										}}
										previousStep={() => {
											!(stepShowSpeech === 2) &&
												setStepShowSpeech(stepShowSpeech - 1);
										}}
										onClickPlay={() => {
											setStepGame(1);
											setValueLoader(1);
										}}
									/>
								</>
							)}
							{stepGame === 1 && (
								<motion.div
									key="memory"
									initial={{ x: 0, opacity: 0 }}
									animate={{ x: 0, opacity: 1 }}
									transition={{ delay: 0.2 }}
									style={{ width: "55%" }}
								>
									<MemoryNumber
										nextStepGame={() => setStepGame((prev) => prev + 1)}
										raiseValueLoader={() => setValueLoader(valueLoader + 6)}
										skipGame={() => setValueLoader(25)}
									/>
								</motion.div>
							)}
							{stepGame === 2 && (
								<motion.div
									key="reaction"
									initial={{ x: 0, opacity: 0 }}
									animate={{ x: 0, opacity: 1 }}
									transition={{ delay: 0.2 }}
								>
									<ReactionClick
										nextStepGame={() => setStepGame((prev) => prev + 1)}
										raiseValueLoader={() => setValueLoader(valueLoader + 25)}
										skipGame={() => setValueLoader(50)}
									/>
								</motion.div>
							)}
							{stepGame === 3 && (
								<motion.div
									key="cognitive"
									initial={{ x: 0, opacity: 0 }}
									animate={{ x: 0, opacity: 1 }}
									transition={{ delay: 0.2 }}
								>
									<CognitiveTest
										nextStepGame={() => setStepGame((prev) => prev + 1)}
										raiseValueLoader={() => setValueLoader(valueLoader + 25)}
										skipGame={() => setValueLoader(75)}
									/>
								</motion.div>
							)}
							{stepGame === 4 && (
								<motion.div
									key="sequence"
									initial={{ x: 0, opacity: 0 }}
									animate={{ x: 0, opacity: 1 }}
									transition={{ delay: 0.2 }}
								>
									<SequenceMemory
										raiseValueLoader={() => setValueLoader(valueLoader + 25)}
										skipGame={() => setValueLoader(75)}
									/>
								</motion.div>
							)}
							<ProgressBar
								stepSpeech={stepShowSpeech}
								valueLoader={valueLoader}
								setValueLoader={(value) => setValueLoader(value)}
								resetValue={stepGame !== 0}
							/>
						</div>
					</div>
				</div>
			) : (
				<div className={styles.responsiveMessage}>
					<div className={styles.content}>
						<div className="flex items-center justify-center space-x-2">
							<div className="w-4 h-4 rounded-full animate-pulse bg-rose-400"></div>
							<div className="w-4 h-4 rounded-full animate-pulse bg-rose-400"></div>
							<div className="w-4 h-4 rounded-full animate-pulse bg-rose-400"></div>
						</div>
						<div className={styles.text}>
							game
							<p className={styles.infos}>
								L'immersion est en cours de développement pour les plus petits
								écrans !
							</p>
							<p className={styles.access}>
								Mais vous pouvez quand même accéder à mon protfolio.
							</p>
						</div>
						<Button
							Icon={RightArrowIcon}
							color="secondary"
							onClick={() => router.push("/portfolio")}
						>
							Accéder au portfolio
						</Button>
					</div>
				</div>
			)}
		</Body>
	) : (
		<></>
	);
}

export default Game;
