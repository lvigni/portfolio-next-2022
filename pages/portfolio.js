import ParticlesArea from "../components/ParticlesArea/ParticlesArea";
import styles from "./portfolio.module.scss";
import { useCallback, useState } from "react";
import Presentation from "../components/portfolio/presentation/Presentation";
import Skills from "../components/portfolio/skills/Skills";
import { motion } from "framer-motion";
import Projects from "../components/portfolio/projects/Projects";
import Button from "../components/Button/Button";
import DownloadIcon from "../components/svg/DownloadIcon";
import History from "../components/portfolio/history/History";
import NextIcon from "../components/svg/NextIcon";
import Body from "../components/body/Body";
import { screenSuperiorOf } from "../utils/utils";

export default function Portfolio() {
	const [labelToShow, setLabelToShow] = useState("");
	const [cellClicked, setCellClicked] = useState(null);

	const getLabel = useCallback(() => {
		switch (labelToShow) {
			case "intro":
				return "Présentation";
			case "history":
				return "Parcours";
			case "skill":
				return "Compétences";
			case "project":
				return "Projets";
			default:
				return "";
		}
	}, [labelToShow]);

	const displayCell = useCallback(
		(label) => {
			return !cellClicked || cellClicked === label;
		},
		[cellClicked]
	);

	const onClickCell = useCallback(
		(label) => {
			if (!cellClicked) {
				setCellClicked(label);
				setLabelToShow(null);
			}
		},
		[cellClicked]
	);

	const handleNextSection = () => {
		switch (cellClicked) {
			case "intro":
				setCellClicked("history");
				break;
			case "history":
				setCellClicked("skill");
				break;
			case "skill":
				setCellClicked("project");
				break;
			case "project":
				setCellClicked("intro");
				break;
		}
	};
	return (
		<Body title="Vigni Lucas - Portfolio">
			<div className={styles.portfolio}>
				{!cellClicked ? (
					<motion.div
						key="download-cv"
						initial={{ x: 0, opacity: 0 }}
						animate={{ x: 0, opacity: 1 }}
						transition={{ delay: 0.1 }}
						className={styles.absoluteBtn}
					>
						<Button
							Icon={DownloadIcon}
							color="secondary"
							onClick={() => window.open("/cv.pdf")}
						>
							Télécharger mon CV
						</Button>
					</motion.div>
				) : (
					<motion.div
						key="next-section"
						initial={{ x: 0, opacity: 0 }}
						animate={{ x: 0, opacity: 1 }}
						transition={{ delay: 0.1 }}
						className={styles.absoluteBtn}
					>
						<Button Icon={NextIcon} onClick={handleNextSection}>
							{screenSuperiorOf(480) ? "Prochaine section" : ""}
						</Button>
					</motion.div>
				)}
				<ParticlesArea />
				<motion.div
					key={`title-${labelToShow}`}
					initial={{ x: 0, opacity: 0 }}
					animate={{ x: 0, opacity: 1 }}
					transition={{ delay: 0.1 }}
				>
					{labelToShow && <h3 className={styles.title}>{getLabel()}</h3>}
				</motion.div>
				<div
					className={`${styles.brainContainer} ${
						cellClicked ? styles.reduce : ""
					}`}
				>
					<div
						className={styles.brain}
						onClick={() => cellClicked && setCellClicked(null)}
					>
						<img className={styles.brainImg} src="bg-game.png" alt="" />
						{displayCell("intro") && (
							<img
								src="/brain/intro-cell-red.png"
								alt=""
								className={`${styles.cell} ${styles.intro} ${
									cellClicked === "intro" ? styles.isShiny : ""
								}`}
								onMouseEnter={() => !cellClicked && setLabelToShow("intro")}
								onMouseLeave={() => setLabelToShow(null)}
								onClick={() => onClickCell("intro")}
							/>
						)}
						{displayCell("history") && (
							<img
								src="/brain/parcours-cell-red.png"
								alt=""
								className={`${styles.cell} ${styles.history} ${
									cellClicked === "history" ? styles.isShiny : ""
								}`}
								onMouseEnter={() => !cellClicked && setLabelToShow("history")}
								onMouseLeave={() => setLabelToShow(null)}
								onClick={() => onClickCell("history")}
							/>
						)}
						{displayCell("skill") && (
							<img
								src="/brain/skills-cell-red.png"
								alt=""
								className={`${styles.cell} ${styles.skill} ${
									cellClicked === "skill" ? styles.isShiny : ""
								}`}
								onMouseEnter={() => !cellClicked && setLabelToShow("skill")}
								onMouseLeave={() => setLabelToShow(null)}
								onClick={() => onClickCell("skill")}
							/>
						)}
						{displayCell("project") && (
							<img
								src="/brain/projects-cell-red.png"
								alt=""
								className={`${styles.cell} ${styles.project} ${
									cellClicked === "project" ? styles.isShiny : ""
								}`}
								onMouseEnter={() => !cellClicked && setLabelToShow("project")}
								onMouseLeave={() => setLabelToShow(null)}
								onClick={() => onClickCell("project")}
							/>
						)}
					</div>
				</div>
				{cellClicked === "intro" && <Presentation />}
				{cellClicked === "history" && <History />}
				{cellClicked === "skill" && <Skills />}
				{cellClicked === "project" && <Projects />}
			</div>
		</Body>
	);
}
