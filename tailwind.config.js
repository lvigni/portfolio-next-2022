/** @type {import('tailwindcss').Config} */
module.exports = {
	important: true,
	content: [
		"./pages/**/*.{js,ts,jsx,tsx}",
		"./components/**/*.{js,ts,jsx,tsx}",
		"./node_modules/flowbite-react/**/*.{js,jsx,ts,tsx}",
	],
	theme: {
		extend: {},
		screens: {
			mobile: "320px",
			tablet: "480px",
			laptop: "768px",
			large: { max: "1050px" },
		},
	},
	plugins: [require("flowbite/plugin")],
};
