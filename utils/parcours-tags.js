const esgiTags = [
	"JS",
	"NodeJS",
	"React",
	"React Native",
	"PHP",
	"Symfony",
	"Api platform",
	"Python",
	"Django",
	"GO",
	"PWA",
	"Firebase",
	"ELK",
	"Tests de performance",
	"Tests unitaires et fonctionnels",
	"Déploiement continue",
	"Docker",
	"Trello",
	"Intégration",
	"Git",
];

const masterTags = [
	"JS",
	"Intégration",
	"Angular",
	"React",
	"PHP",
	"Symfony",
	"C#",
	"Gestion de projet",
	"Communication",
	"Test unitaires et fonctionnels",
];

const licenceTags = [
	"JS",
	"Intégration",
	"Angular",
	"React",
	"PHP",
	"Symfony",
	"Gestion de projet",
	"Communication",
	"Recettage",
	"UX / UI",
];
const btsTags = [
	"C#",
	"HTML",
	"CSS",
	"Monde object",
	"Algorithme",
	"SQL",
	"Réseau",
];

const webexprTags = [
	"Nestjs",
	"Symfony",
	"Laravel",
	"Django",
	"React",
	"React native",
	"Vue.js",
	"Git",
	"CI/CD",
	"Environnement serveur",
	"Gestion de projet",
	"Trello",
	"Jira",
];

const airFranceTags = [
	"Angular",
	"Recueil de besoins",
	"Access",
	"Analyse des KPI",
	"Documentations",
];

const oxyleadTags = ["Microsoft 365", "Ticketing", "TMA", "TME"];

const avrTags = ["Tests front", "Nightmare.js", "JS"];

const lenormantTags = ["Hardware", "Software", "PHP", "JS"];

export {
	esgiTags,
	masterTags,
	licenceTags,
	btsTags,
	webexprTags,
	airFranceTags,
	oxyleadTags,
	avrTags,
	lenormantTags,
};
