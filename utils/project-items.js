const DescriptionAxotools = () => {
	return (
		<div>
			<p>
				Axotools est une solution digitale pour les associations{" "}
				<span>E-Sportives</span>.
			</p>
			<p>
				L’objectif est de <span>centraliser</span> les données globales
				(joueurs, équipes, documents…), avec un dashboard détaillé afin de
				faciliter la gestion de la structure.
			</p>
			<p>
				Il y a 3 interfaces: Une application <span>web</span>, un site{" "}
				<span>vitrine</span> et une application <span>mobile</span>.
			</p>
			<p>
				L’objectif est de satisfaire les besoins de la structure pour{" "}
				<span>éviter la répartition</span> des informations et le management
				entre plusieurs outils (drive, excel, discord, trello etc).
			</p>
		</div>
	);
};

const DescriptionMogador = () => {
	return (
		<div>
			<p>
				Mogador app event est une application permettant la création{" "}
				<span>d'événements personnalisés</span>, avec génération de site
				référencer et envoies d'emails.
			</p>
			<p>
				La <span>gestion</span> des événements est <span>profonde</span> avec la
				personnalisation du contenu du site, des organisateurs, l'ajout
				d'invités de resources/médias, d'administration des <span>emails</span>{" "}
				automatiques ou manuel, etc.
			</p>
			<p>
				Un site en <span>nextjs</span> est relié et référencé pour chaque
				événement, permettant aux invités et visiteurs de <span>s'inscire</span>{" "}
				ou de <span>consulter</span> son détail.
			</p>
			<p>
				D'abord utilisée en internedans mon ancienne agence, cette application a
				pour but d'être utilisé en mode <span>SaaS</span>, donnant à chaque
				entreprise, son <span>environnement dédié</span>.
			</p>
		</div>
	);
};

const DescriptionSparklanes = () => {
	return (
		<div>
			<p>
				Sparklanes est une application de gestion de <span>transports</span> et{" "}
				<span>d'excursions</span>.
			</p>
			<p>
				Elle possède 3 interfaces <span>web</span>; administrateur, prestataire
				et client.
			</p>
			<p>
				Aussi, une application <span>mobile</span> est disponible pour les
				chauffeurs, afin d'avoir un accès total a ses transferts et de remonter
				les informations sur l'application web.
			</p>
			<p>
				Elle permet une <span>gestion complète</span> d'un dossier, de la
				création des transferts à la prise en charge de la course avec{" "}
				<span>plusieurs</span>
				niveaux de <span>validation</span> entre les interfaces.
			</p>
		</div>
	);
};

const DescriptionIcad = () => {
	return (
		<div>
			<p>
				Icad est une application permettant de <span>recenser</span> tous les
				chiens de protection.
			</p>
			<p>
				Pour enregistrer un chien, il faut saisir{" "}
				<span>toutes ses informations</span> : leur environnement, maître,
				exploitation et propriétaire.
			</p>
			<p>
				Le but de l'application est de <span>faciliter</span> le processus de
				recensement grâce à plusieurs features pouvant <span>automatiser</span>{" "}
				et <span>faciliter</span> le stockage d'informations.
			</p>
		</div>
	);
};

// const DescriptionJanus = () => {
// 	return (
// 		<div>
// 			<p>
// 				Janus est une application mobile ipad, destiné aux professionnels de la{" "}
// 				<span>menuiserie</span>.
// 			</p>
// 			<p>
// 				L'objectif de la solution est de <span>faciliter</span> le travail des
// 				menuisiers afin d'augmenter leur <span>productivité</span>.
// 			</p>
// 			<p>
// 				Elle permet de <span>centraliser</span> leurs interventions
// 				quotidiennes, remonter des informations importantes, réaliser des prises
// 				de côtes, afficher les stocks etc.
// 			</p>
// 		</div>
// 	);
// };

const projectItems = [
	{
		id: 1,
		title: "CRM - Esport",
		subtitle: "Axotools",
		label: "perso",
		description: <DescriptionAxotools />,
		pictures: [
			"/projects/axotools/axotools-login.png",
			"/projects/axotools/axotools-paiements.png",
			"/projects/axotools/axotools-dashboard-white.png",
			"/projects/axotools/axotools-edit-profil.png",
			"/projects/axotools/axotools-detail-user.png",
			"/projects/axotools/axotools-list-teams.png",
			"/projects/axotools/axotools-choice.png",
			"/projects/axotools/axotools-candidate.png",
		],
		features: [
			"Double authentification",
			"Moyent de paiement par Stripe",
			"Dashboard détaillé: Api twitch, graphs, KPI",
			"Traduction avec i18n",
			"Dark mode",
		],
		technos: [
			"/skills/docker.webp",
			"/skills/symfony.png",
			"/skills/api-platform.svg",
			"/skills/phpunit.webp",
			"/skills/postgres.png",
			"/skills/react.svg.png",
		],
	},
	{
		id: 2,
		title: "Gestion - Évènements",
		subtitle: "Mogador",
		label: "pro",
		description: <DescriptionMogador />,
		pictures: [
			"/projects/mogador/mogador-login.PNG",
			"/projects/mogador/mogador-accounts.png",
			"/projects/mogador/mogador-list-events.png",
			"/projects/mogador/mogador-detail-event.png",
			"/projects/mogador/mogador-guests.png",
			"/projects/mogador/mogador-settings.png",
			"/projects/mogador/mogador-next1.PNG",
			"/projects/mogador/mogador-next-register.PNG",
			"/projects/mogador/mogador-next2.PNG",
		],
		features: [
			"Emails / formulaires administrable",
			"Imports csv",
			"App nextjs binder à un événement",
			"Mode saas",
			"Beaucoup de tests front/back",
		],
		technos: [
			"/skills/docker.webp",
			"/skills/nestjs.png",
			"/skills/jest.png",
			"/skills/mongo.png",
			"/skills/react.svg.png",
			"/skills/nextjs.png",
			"/skills/cypress.webp",
		],
	},
	{
		id: 3,
		title: "ERP - Transports & excursions",
		subtitle: "Sparklanes",
		label: "pro",
		description: <DescriptionSparklanes />,
		pictures: [
			"/projects/sparklanes/sparklanes-dashboard-client.png",
			"/projects/sparklanes/sparklanes-dossier-client.png",
			"/projects/sparklanes/sparklanes-transferts-client.png",
			"/projects/sparklanes/sparklanes-client-tracking.png",
			"/projects/sparklanes/sparklanes-vehicles.png",
			"/projects/sparklanes/sparklanes-chauffeur.png",
			"/projects/sparklanes/sparklanes-devis.png",
			"/projects/sparklanes/sparklanes-dossiers.png",
			"/projects/sparklanes/sparklanes-dashboard.png",
			"/projects/sparklanes/sparklanes-clients.png",
			"/projects/sparklanes/sparklanes-transferts-mobile.png",
			"/projects/sparklanes/sparklanes-transfert-mobile.png",
			"/projects/sparklanes/sparklanes-appeloffre-mobile.png",
		],
		features: [
			"Google map",
			"Devis et factures administrable",
			"Dashboard complet",
		],
		technos: [
			"/skills/docker.webp",
			"/skills/laravel.png",
			"/skills/phpunit.webp",
			"/skills/postgres.png",
			"/skills/react.svg.png",
		],
	},
	{
		id: 4,
		title: "ERP - Identification des chiens",
		subtitle: "ICAD",
		label: "pro",
		description: <DescriptionIcad />,
		pictures: [
			"/projects/icad/icad-add-dog.png",
			"/projects/icad/icad-master.png",
			"/projects/icad/icad-list-to-sync.png",
			"/projects/icad/icad-modal-to-sync.png",
		],
		features: [
			"Formulaires poussés",
			"Mode hors connection",
			"Trame de validation",
		],
		technos: [
			"/skills/docker.webp",
			"/skills/symfony.png",
			"/skills/api-platform.svg",
			"/skills/vuejs.png",
		],
	},
	// {
	// 	id: 4,
	// 	title: "Gestion - Maçonnerie",
	// 	subtitle: "Janus",
	// 	label: "pro",
	// 	description: <DescriptionJanus />,
	// 	pictures: [
	// 		"/projects/janus/janus-listing-inters.png",
	// 		"/projects/janus/janus-step1.png",
	// 		"/projects/janus/janus-listing-tasks.png",
	// 		"/projects/janus/janus-add-product.png",
	// 		"/projects/janus/janus-cotes.png",
	// 	],
	// 	features: [
	// 		"Chronométres et temps total",
	// 		"Google map",
	// 		"Formulaires poussé: Prise de cotes",
	// 	],
	// 	technos: [
	// 		"/skills/docker.webp",
	// 		"/skills/symfony.png",
	// 		"/skills/api-platform.svg",
	// 		"/skills/react.svg.png",
	// 	],
	// },
];

export default projectItems;
