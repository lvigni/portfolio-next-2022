import ImageLabel from "../components/image-label/ImageLabel";
import { screenSuperiorOf } from "./utils";

const frameworkMedias = [
	<ImageLabel
		path="/skills/react.svg.png"
		label="React & React Native"
		link="https://fr.reactjs.org/"
	/>,
	<ImageLabel
		path="/skills/nextjs.png"
		label="Next.js"
		link="https://nextjs.org/"
	/>,
	<ImageLabel
		path="/skills/vuejs.png"
		label="Vue.js"
		link="https://vuejs.org/"
	/>,
	<ImageLabel
		path="/skills/redux.png"
		label="Redux"
		link="https://redux.js.org/"
	/>,
	<ImageLabel
		path="/skills/docker.webp"
		label="Docker"
		link="https://www.docker.com/"
	/>,
	<ImageLabel
		path="/skills/nestjs.png"
		label="NestJS"
		link="https://nestjs.com/"
	/>,
	<ImageLabel
		path="/skills/nodejs.png"
		label="Node.js"
		link="https://nodejs.org/en/"
	/>,
	<ImageLabel
		path="/skills/symfony.png"
		label="Symfony & Api patform"
		link="https://symfony.com/"
	/>,
	<ImageLabel
		path="/skills/django.png"
		label="Django"
		link="https://www.djangoproject.com/"
	/>,
	<ImageLabel
		path="/skills/laravel.svg.png"
		label="Laravel"
		link="https://laravel.com/"
	/>,
	// <ImageLabel path="/skills/api-platform.svg" label="Api platform" />,
];

const languagesMedias = [
	<ImageLabel
		path="/skills/js.png"
		label="JavaScript"
		link="https://www.mysql.com/fr/"
	/>,
	<ImageLabel
		path="/skills/ts.svg.png"
		label="TypeScript"
		link="https://www.postgresql.org/"
	/>,
	<ImageLabel
		path="/skills/php.png"
		label="PHP"
		width={screenSuperiorOf("480") ? "120px" : "80px"}
		link="https://www.postgresql.org/"
	/>,
	<ImageLabel
		path="/skills/python.png"
		label="Python"
		link="https://www.mongodb.com/fr-fr"
	/>,
	<ImageLabel
		path="/skills/html.png"
		label="HTML"
		link="https://www.mongodb.com/fr-fr"
	/>,
	<ImageLabel
		path="/skills/css.png"
		label="CSS"
		link="https://www.mongodb.com/fr-fr"
	/>,
	<ImageLabel
		path="/skills/scss.png"
		label="SCSS"
		link="https://www.mongodb.com/fr-fr"
	/>,
	<ImageLabel
		path="/skills/sql.png"
		label="SQL"
		link="https://www.mongodb.com/fr-fr"
	/>,
];

const bddMedias = [
	<ImageLabel
		path="/skills/mysql.png"
		label="MySQL"
		link="https://www.mysql.com/fr/"
	/>,
	<ImageLabel
		path="/skills/postgres.png"
		label="PostgreSQL"
		link="https://www.postgresql.org/"
	/>,
	<ImageLabel
		path="/skills/mongo.png"
		label="MongoDB"
		link="https://www.mongodb.com/fr-fr"
	/>,
];

const versioningMedias = [
	<ImageLabel path="/github.png" label="Github" link="https://github.com/" />,
	<ImageLabel
		path="/skills/gitlab.png"
		label="Gitlab"
		link="https://about.gitlab.com/"
	/>,
	<ImageLabel
		path="/skills/ci-cd.png"
		width={screenSuperiorOf("480") ? "110px" : "80px"}
		link="https://fr.wikipedia.org/wiki/CI/CD"
	/>,
];

const testMedias = [
	<ImageLabel
		path="/skills/jest.png"
		label="Jest"
		link="https://jestjs.io/fr/"
	/>,
	<ImageLabel
		path="/skills/cypress.webp"
		label="Cypress"
		link="https://www.cypress.io/"
	/>,
	<ImageLabel
		path="/skills/phpunit.webp"
		width={screenSuperiorOf("480") ? "140px" : "80px"}
		link="https://phpunit.de/"
	/>,
];

const plusMedias = [
	<ImageLabel
		path="/skills/slack.png"
		label="Slack"
		link="https://slack.com/intl/fr-fr"
	/>,
	<ImageLabel
		path="/skills/trello.png"
		label="Trello"
		link="https://trello.com/fr"
	/>,
	<ImageLabel
		path="/skills/jira.png"
		label="Jira"
		link="https://www.atlassian.com/fr/software/jira"
	/>,
];

export {
	frameworkMedias,
	languagesMedias,
	bddMedias,
	testMedias,
	plusMedias,
	versioningMedias,
};
