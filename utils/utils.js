const randomNumber = (min, max) => {
	return Math.floor(Math.random() * (max - min + 1) + min);
};

const screenSuperiorOf = (px) => {
	console.log(typeof window !== "undefined" && window.innerWidth);
	// console.log(screenSuperiorOf(480));
	return typeof window !== "undefined" && window.innerWidth >= px;
};
export { randomNumber, screenSuperiorOf };
